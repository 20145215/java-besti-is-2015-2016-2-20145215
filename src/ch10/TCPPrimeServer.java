package ch10;
import java.net.*;
/**
 * Created by Think on 2016/5/8.
 */
public class TCPPrimeServer {
    public static void main(String[] args) {
        final int PORT = 10005;
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            System.out.println("服务器端已启动：");
            while(true){
                Socket s = ss.accept();
                new PrimeLogicThread(s);
            }
        } catch (Exception e) {}
        finally{
            try {
                ss.close();
            } catch (Exception e2) {}
        }

    }
}
