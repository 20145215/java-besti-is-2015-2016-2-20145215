package ch10;
import java.net.*;
/**
 * Created by Think on 2016/5/8.
 */
public class TCPServer {
    public static void main(String[] args) {
        try{
            //监听端口
            ServerSocket ss = new ServerSocket(10001);
            System.out.println("服务器已启动：");
            //逻辑处理
            while(true){
                //获得连接
                Socket s = ss.accept();
                //启动线程处理
                new LogicThread3(s);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

