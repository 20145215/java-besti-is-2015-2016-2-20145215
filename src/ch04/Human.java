package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class Human implements Swimmer {
    private String name;
    public Human(String name){
        this.name=name;
    }
    public String getName()
    {
        return name;
    }


    @Override
    public void swim()
    {
        System.out.printf("人类 %s 游泳 %n",name);
    }
}
