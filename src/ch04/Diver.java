package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public interface Diver extends Swimmer
{
    public abstract void dive();
}
