package ch04;

/**
 * Created by Think on 2016/3/26.
 */
public class SuperTest {
    public static void main(String[] args) {
        new SubClass().showMessage();
    }
}
   class SuperClass {
    int i = 10;
}
 class SubClass extends SuperClass {
    int i = 20;
    public void showMessage() {
        System.out.printf("super.i = %d, this.i = %d\n", super.i, this.i);
    }
}
