package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class Anemonefish extends Fish
{
    public Anemonefish(String name)
    {
        super(name);
    }
    @Override
    public void swim()
    {
        System.out.printf("小丑鱼 %s 游泳%n",name);
    }
}
