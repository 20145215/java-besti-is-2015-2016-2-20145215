package ch04;
public class RPG1
{
	public static void main (String[] args)
	{
		demoSwordsMan();
		demoMagician();
	}
	static void demoSwordsMan()
	{
		SwordsMan1 swordsMan1 = new SwordsMan1();
		swordsMan1.setName("Justin");
		swordsMan1.setLevel(1);
		swordsMan1.setBlood(200);
		System.out.printf("剑士 : (%s, %d, %d)%n",swordsMan1.getName(),
				swordsMan1.getLevel(),swordsMan1.getBlood());
	}
	static void demoMagician()
	{
		Magician1 magician = new Magician1();
		magician.setName("Moinca");
		magician.setLevel(1);
		magician.setBlood(100);
		System.out.printf("魔法师 :(%s ,%d ,%d)%n",magician.getName(),
				magician.getLevel(),magician.getBlood());
		
	}
}