package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class SwimPlayer extends Human2 implements Swimmer
{
    public SwimPlayer(String name)
    {
        super(name);
    }

    @Override
    public void swim()
    {
        System.out.printf("游泳选手 %s 游泳%n",name);
    }
}
