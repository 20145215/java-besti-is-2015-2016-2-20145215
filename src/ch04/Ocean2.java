package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class Ocean2{
    public static void main(String[] args)
    {
        doSwim(new Anemonefish("尼莫"));
        doSwim(new Shark("兰尼"));
        doSwim(new Human("贾斯汀"));
        doSwim(new Submarine("黄色一号"));
        doSwim(new Seaplane("空军零号"));
        doSwim(new FlyingFish("甚平"));
    }

    static void doSwim(Swimmer swimmer)
    {
        swimmer.swim();
    }
}