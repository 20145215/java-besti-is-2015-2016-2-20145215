package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public abstract class Fish implements Swimmer {
    protected String name;
    public Fish(String name){
        this.name = name;
    }
    public String getName()
    {
        return name;
    }
    @Override
    public abstract void swim();
}
