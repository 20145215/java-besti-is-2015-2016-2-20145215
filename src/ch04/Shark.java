package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class Shark extends Fish
{
    public Shark(String name)
    {
        super(name);
    }
    @Override
    public void swim()
    {
        System.out.printf("鲨鱼 %s 游泳%n",name);
    }
}
