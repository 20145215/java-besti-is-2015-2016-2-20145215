package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public interface ClientListener{
    void clientAdded(ClientEvent event);
    void clientRemoved(ClientEvent event);
}