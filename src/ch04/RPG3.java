package ch04;
public class RPG3
{
	public static void main(String[] args)
	{
		SwordsMan3 swordsMan3 = new SwordsMan3();
		swordsMan3.setName("Justin");
		swordsMan3.setLevel(1);
		swordsMan3.setBlood(200);
		
		Magician3 magician3 = new Magician3();
		magician3.setName("Monica");
		magician3.setLevel(1);
		magician3.setBlood(100);
		
		drawFight(swordsMan3);
		drawFight(magician3);
		
	}
	static void drawFight(Role3 role)
	{
		System.out.print(role.getName());
		role.fight3();
	}
}