package ch04;

/**
 * Created by Think on 2016/3/26.
 */
public class OverrideTest {
    public static void main(String[] args) {
    }
}
class SuperClass1 {
    public void f() {
    }
}
class SubClass1 extends SuperClass1 {
    @Override
    public void f() {
        System.out.println("Hello");
    }
}
