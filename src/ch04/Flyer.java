package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public interface Flyer
{
    public abstract void fly();
}
