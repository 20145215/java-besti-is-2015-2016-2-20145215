package ch04;
/**
 * Created by beautiful luna on 2016/3/24.
 */
public class Submarine2 extends Boat implements Diver{
    public Submarine2(String name){
        super(name);
    }


    @Override
    public void dive(){
        System.out.printf("潜水艇 %s 潜行 %n",name);
    }
}
