package ch08;
/**
 * Created by Think on 2016/4/23.
 */
import java.util.logging.*;

public class LoggerDemo2 {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(LoggerDemo.class.getName());
        logger.setLevel(Level.FINE);
        for(Handler handler : logger.getParent().getHandlers()){
            handler.setLevel(Level.FINE);
        }
        logger.log(Level.WARNING, "WARNING 讯息");
        logger.log(Level.INFO, "INFO 讯息");
        logger.log(Level.CONFIG, "CONFIG 讯息");
        logger.log(Level.FINE, "FINE 讯息");
    }
}