package ch08;

/**
 * Created by Think on 2016/4/24.
 */
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.*;
public class Ls2 {
    public static void main(String[] args) throws IOException{

        String syntax = args.length == 2 ? args[0] : "glob";
        String pattern = args.length == 2 ? args[1] : "*";
        out.println(syntax + ":" + pattern);

        Path userPath = Paths.get(System.getProperty("user.dir"));
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(syntax + ":" + pattern);
        try(DirectoryStream<Path> directoryStream = Files.newDirectoryStream(userPath)){
            directoryStream.forEach(path -> {
                Path file = Paths.get(path.getFileName().toString());
                if(matcher.matches(file)){
                    out.println(file.getFileName());
                }
            });
        }
    }
}
