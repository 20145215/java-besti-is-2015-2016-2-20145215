package ch08;

/**
 * Created by Think on 2016/4/24.
 */

import java.nio.file.FileSystems;
import java.nio.file.Path;

import static java.lang.System.out;

public class Root {
    public static void main(String[] args) {
        Iterable<Path> dirs = FileSystems.getDefault().getRootDirectories();
        dirs.forEach(out::println);
    }
}
