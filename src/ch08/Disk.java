package ch08;

/**
 * Created by Think on 2016/4/24.
 */

import java.io.IOException;
import java.nio.file.*;
import java.text.DecimalFormat;

import static java.lang.System.out;

public class Disk {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            FileSystem fs = FileSystems.getDefault();
            for (FileStore store : fs.getFileStores()) {
                print(store);
            }
        } else {
            for (String file : args) {
                FileStore store = Files.getFileStore(Paths.get(file));
                print(store);
            }
        }
    }

    public static void print(FileStore store) throws IOException {
        long total = store.getTotalSpace();
        long used = store.getTotalSpace() - store.getUnallocatedSpace();
        long usable = store.getUsableSpace();
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        out.println(store.toString());
        out.printf("\t- all space\t%s\tbyte%n", formatter.format(total));
        out.printf("\t- space can be used\t%s\tbyte%n", formatter.format(used));
        out.printf("\t- space used\t%s\tbyte%n", formatter.format(usable));
    }
}
