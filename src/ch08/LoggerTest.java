package ch08;
import java.util.logging.*;
/**
 * Created by Think on 2016/4/24.
 */
public class LoggerTest {
    public static void main(String[] args) {
        Logger log = Logger.getLogger("lavasoft" );
        log.setLevel(Level.INFO);
        Logger log1 = Logger.getLogger("lavasoft" );
        System.out.println(log==log1);     //true
        Logger log2 = Logger.getLogger("lavasoft.blog" );
        //log2.setLevel(Level.WARNING);

        log.info("aaa" );
        log2.info("bbb" );
        log2.fine("fine" );
    }
}
