package ch08;

/**
 * Created by Think on 2016/4/24.
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import static java.lang.System.out;

public class BasicFileAttributesDemo {
    public static void main(String[] args) throws IOException {
        Path file = Paths.get("C:\\Windows");
        BasicFileAttributes attrs = Files.readAttributes(file, BasicFileAttributes.class);
        out.printf("creationTime: %s%n", attrs.creationTime());
        out.printf("lastAccessTime: %s%n", attrs.lastAccessTime());
        out.printf("lastModifiedTime: %s%n", attrs.lastModifiedTime());
        out.printf("isDirectory: %b%n", attrs.isDirectory());
        out.printf("isOther: %b%n", attrs.isOther());
        out.printf("isRegularFile: %b%n", attrs.isRegularFile());
        out.printf("isSymbolicLink: %b%n", attrs.isSymbolicLink());
        out.printf("size: %d%n", attrs.size());
    }
}
