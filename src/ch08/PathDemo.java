package ch08;

/**
 * Created by Think on 2016/4/24.
 */

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.out;

public class PathDemo {
    public static void main(String[] args) {
        Path path = Paths.get(System.getProperty("user.home"), "Documents", "Downloads");
        out.printf("toString: %s%n", path.toString());
        out.printf("getFileName: %s%n", path.getFileName());
        out.printf("getName: %s%n", path.getName(0));
        out.printf("getNameCount: %d%n", path.getNameCount());
        out.printf("subpath: %s%n", path.subpath(0, 2));
        out.printf("getParent: %s%n", path.getParent());
        out.printf("getRoot: %s%n", path.getRoot());
    }
}
