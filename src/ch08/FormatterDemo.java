package ch08;
/**
 * Created by Think on 2016/4/23.
 */

import java.time.Instant;
import java.util.logging.*;

public class FormatterDemo {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(FormatterDemo.class.getName());
        logger.setLevel(Level.CONFIG);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.CONFIG);
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return "日志来自 " + record.getSourceClassName() + "."
                        + record.getSourceMethodName() + "\n"
                        + "\t层级\t: " + record.getLevel() + "\n"
                        + "\t讯息\t: " + record.getMessage() + "\n"
                        + "\t时间\t: "  + Instant.ofEpochMilli(record.getMillis())
                        + "\n";
            }
        });
        logger.addHandler(handler);
        logger.config(" Formatter 讯息");
    }
}
