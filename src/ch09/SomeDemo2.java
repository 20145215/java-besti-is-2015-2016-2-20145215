package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import static java.lang.System.out;

public class SomeDemo2 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class clz = Class.forName("CH9.Unit17.Some2", false, SomeDemo2.class.getClassLoader());
        out.println("已载入 Some2.class");
        Some2 s;
        out.println("声明 Some 参考名称");
        s = new Some2();
        out.println("生成 Some 实例");
    }
}

class Some2 {
    static {
        out.println("[执行静态区块]");
    }
}
