package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import static java.lang.System.out;

public class InfoAbout {
    public static void main(String[] args) {
        try {
            Class clz = Class.forName(args[0]);
            out.println("类名称：" + clz.getName());
            out.println("是否为接口:" + clz.isInterface());
            out.println("是否为基本类型：" + clz.isPrimitive());
            out.println("是否为数组对象：" + clz.isArray());
            out.println("父类名称：" + clz.getSuperclass().getName());
        } catch (ArrayIndexOutOfBoundsException e) {
            out.println("没有指定类名称");
        } catch (ClassNotFoundException e) {
            out.println("找不到指定的类" + args[0]);
        }
    }
}
