package ch09;

/**
 * Created by Think on 2016/4/30.
 */
public interface Player {
    void play(String video);
}
