package ch09;

import java.lang.reflect.Array;

public class ArrayList<E> {
    private Object[] elems;
    private int next;

    public ArrayList(int capacity) {
        elems = new Object[capacity];
    }

    public ArrayList() {
        this(16);
    }

    public E[] toArray() {
        E[] elements = null;
        if (size() > 0) {
            elements = (E[]) Array.newInstance(elems[0].getClass(), size());
            for (int i = 0; i < elements.length; i++) {
                elements[i] = (E) elems[i];
            }
        }
        return elements;
    }

    private int size() {
        return size();
    }
}
