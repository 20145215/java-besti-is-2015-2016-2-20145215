package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import static java.lang.System.out;

public class SomeDemo {
    public static void main(String[] args) {
        Some s;
        out.println("声明 Some 参考名称");
        s = new Some();
        out.println("生成 Some 实例");
    }
}
