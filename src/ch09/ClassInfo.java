package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import static java.lang.System.out;

public class ClassInfo {
    public static void main(String[] args) {
        Class clz = String.class;
        out.println("类名称：" + clz.getName());
        out.println("是否为接口:" + clz.isInterface());
        out.println("是否为基本类型：" + clz.isPrimitive());
        out.println("是否为数组对象：" + clz.isArray());
        out.println("父类名称：" + clz.getSuperclass().getName());
    }
}
