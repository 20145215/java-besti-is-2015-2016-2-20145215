package ch09;

/**
 * Created by Think on 2016/4/30.
 */
public class ProxyDemo {
    public static void main(String[] args) {
        LoggingHandler loggingHandler = new LoggingHandler();
        Hello helloProxy = (Hello) loggingHandler.bind(new HelloSpeaker());
        helloProxy.hello("Cai");
    }
}
