package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import static java.lang.System.out;

public class ClassLoaderHierarchy {
    public static void main(String[] args) {
        Some some = new Some();
        Class clz = some.getClass();
        ClassLoader loader = clz.getClassLoader();
        out.println(loader);
        out.println(loader.getParent());
        out.println(loader.getParent().getParent());
    }
}
