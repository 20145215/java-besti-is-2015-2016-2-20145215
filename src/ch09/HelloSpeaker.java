package ch09;

/**
 * Created by Think on 2016/4/30.
 */
public class HelloSpeaker implements Hello {
    public void hello(String name) {
        System.out.printf("你好，%s%n", name);
    }
}
