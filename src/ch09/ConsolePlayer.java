package ch09;

/**
 * Created by Think on 2016/4/30.
 */
public class ConsolePlayer implements Player {
    @Override
    public void play(String video) {
        System.out.println("正在播放" + video);
    }
}
