package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import static java.lang.System.out;
public class ClassLoaderDemo {
    public static void main(String[] args) {
        try {
            String path = args[0];
            String clzName = args[1];
            Class clz1 = loadClassFrom(path, clzName);
            out.println(clz1);
            Class clz2 = loadClassFrom(path, clzName);
            out.println(clz2);
            out.printf("clz1与clz2为%s实例", clz1 == clz2 ? "相同" : "不同");
        } catch (ArrayIndexOutOfBoundsException e) {
            out.println("没有指定类加载路径与名称");
        } catch (MalformedURLException e) {
            out.println("加载路径错误");
        } catch (ClassNotFoundException e) {
            out.println("找不到指定类");
        }
    }

    private static Class loadClassFrom(String path, String clzName) throws ClassNotFoundException, MalformedURLException {
        ClassLoader loader = new URLClassLoader(new URL[]{new URL(path)});
        return loader.loadClass(clzName);
    }
}
