package ch09;

/**
 * Created by Think on 2016/4/27.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static java.lang.System.out;

public class ConnectionDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String jdbcUrl = "jdbc:mysql://localhost:3306/lesson";
        String user = "root";
        String passwd = "";
        try (Connection conn = DriverManager.getConnection(jdbcUrl, user, passwd)) {
            out.printf("已%s 数据库联机%n", conn.isClosed() ? "关闭" : "开启");
        }
    }

}
