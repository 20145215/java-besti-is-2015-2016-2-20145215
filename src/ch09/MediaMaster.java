package ch09;

/**
 * Created by Think on 2016/4/30.
 */

import java.util.Scanner;

public class MediaMaster {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        String playerImpl = System.getProperty("CH9.Unit17.PlayerImpl");
        Player player = (Player) Class.forName(playerImpl).newInstance();
        System.out.print("输入想播放的影片：");
        player.play(new Scanner(System.in).nextLine());
    }
}
