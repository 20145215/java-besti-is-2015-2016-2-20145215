﻿package exp03;

public class MyComplex {
    private static int r;
    private static int i;
    private double m;
    private double n;
    public static int getRealPart(int RealPart){
        setR(RealPart);
        return getR();
    }
    public static int getImaginePart(int ImaginePart){
        setI(ImaginePart);
        return getI();
    }
    public MyComplex(double m, double n) {
        this.setM(m);
        this.setN(n);
    }

    public static int getR() {
        return r;
    }

    public static void setR(int r) {
        MyComplex.r = r;
    }

    public static int getI() {
        return i;
    }

    public static void setI(int i) {
        MyComplex.i = i;
    }

    public MyComplex add(MyComplex c) {
        return new MyComplex(getM() + c.getM(), getN() + c.getN());
    }
    public MyComplex minus(MyComplex c) {
        return new MyComplex(getM() - c.getM(), getN() - c.getN());
    }
    public MyComplex multiply(MyComplex c) {
        return new MyComplex(getM() * c.getM() - getN() * c.getN(), getM() * c.getN() + getN() * c.getM());
    }
    public String toString() {
        String rtr_str = "";
        if (getN() > 0)
            rtr_str = "(" + getM() + "+" + getN() + "i" + ")";
        if (getN() == 0)
            rtr_str = "(" + getM() + ")";
        if (getN() < 0)
            rtr_str = "(" + getM() + getN() + "i" + ")";
        return rtr_str;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }
}
