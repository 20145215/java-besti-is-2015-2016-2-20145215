package exp03;

/**
 * Created by Think on 2016/4/24.
 */
Complex类要输出实部，输出虚部，并按照a+bi的形式输出复数。
        Complex类中有两个变量，实部RealPart和虚部ImaginePart；

        方法：
        getRealPart(int RealPart);返回实部
        getImaginePart(int ImaginePart);返回虚部
        add(Complex c);实现复数相加
        minus(Complex c);实现复数相减
        multiply(Complex c);实现复数相乘
        toString(int RealPart,int ImaginePart);将复数输出成a+bi的格式。
