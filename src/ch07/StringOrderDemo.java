package ch07;

import java.util.Arrays;

/**
 * Created by Think on 2016/4/16.
 */
public class StringOrderDemo {
    public static void main(String[] args) {
        String[] names = {"Justin", "caterpillar", "Bush"};
        Arrays.sort(names, StringOrder::byLength);
        System.out.println(Arrays.toString(names));
    }
}
