package ch07;

import java.util.Arrays;

/**
 * Created by Think on 2016/4/16.
 */
public class LamabdaDemo2 {
    public static void main(String[] args) {
        String[] names = {"Justin", "caterpillar", "Bush"};
        Arrays.sort(names, (name1, name2) -> name2.length() - name1.length());
        System.out.println(Arrays.toString(names));
    }
}
