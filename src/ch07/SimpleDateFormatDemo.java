package ch07;

/**
 * Created by Think on 2016/4/16.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateFormatDemo {
    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat(
                args.length == 0 ? "EE-MM-dd-yyyy" : args[8]
        );
        System.out.println(dateFormat.format(new Date()));
    }
}
