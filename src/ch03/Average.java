package ch03;
/**
 * Created by Think on 2016/3/19.
 */
public class Average {
    public static void main(String[] args) {
        long sum=0;
        for(String arg:args){
            sum += Long.parseLong(arg);
        }
        System.out.println("平均："+(float) sum/args.length);
    }
}
