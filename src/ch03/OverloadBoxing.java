package ch03;

/**
 * Created by Think on 2016/3/20.
 */
class Some{
    void someMethod(int i){
        System.out.println("int 版本被调用");
    }
    void someMethod(Integer integer){
        System.out.println("Integer 版本被调用");
    }
}
public class OverloadBoxing {
    public static void main(String[] args) {
        Some s=new Some();
        s.someMethod(1);
    }
}
