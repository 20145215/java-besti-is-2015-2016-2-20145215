package ch03;

/**
 * Created by Think on 2016/3/20.
 */
public class MathTool {
    public static int sum(int... numbers) {
        int sum=0;
        for(int number:numbers){
            sum+=number;
        }
        return sum;
    }
}
