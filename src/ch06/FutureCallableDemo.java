package ch06;
/**
 * Created by Think on 2016/4/9.
 */


import java.util.concurrent.*;
import static java.lang.System.*;

public class FutureCallableDemo {
    static long fibonacci(long n) {
        if (n <= 1) {
            return n;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static void main(String[] args) throws Exception {
        FutureTask<Long> the30thFibFuture =
                new FutureTask<>(() -> fibonacci(30));

        out.println("老板，我要第 30 个费式数，待会来拿...");

        new Thread(the30thFibFuture).start();
        while(!the30thFibFuture.isDone()) {
            out.println("忙別的事去...");
        }

        out.printf("第 30 个费式数：%d%n", the30thFibFuture.get());
    }
}