package ch06;
/**
 * Created by Think on 2016/4/9.
 */
import java.io.*;
public class IO {
    public static void dump(InputStream src, OutputStream dest)
        throws IOException{
        try (InputStream input = src; OutputStream output = dest){

            byte[] data = new byte[1024];
            int length;
            while((length = input.read(data)) != -1)
            {
                output.write(data, 0, length);
            }
        }
    }
}
