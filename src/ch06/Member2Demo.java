package ch06;
/**
 * Created by Think on 2016/4/9.
 */


import static java.lang.System.out;

public class Member2Demo {
    public static void main(String[] args) throws Exception {
        Member2[] members = {
                new Member2("B1234", "Justin", 90),
                new Member2("B5678", "Monica", 95),
                new Member2("B9876", "Irene", 88),
                new Member2("B5220","韩喜斐",78),
                new Member2("B5208","Boss Cai",85),
                new Member2("B5230","Av Bear",88),
        };
        for(Member2 member : members) {
            member.save();
        }
        out.println(Member2.load("B1234"));
        out.println(Member2.load("B5678"));
        out.println(Member2.load("B9876"));
        out.println(Member2.load("B5220"));
        out.println(Member2.load("B5208"));
        out.println(Member2.load("B5230"));
    }
}