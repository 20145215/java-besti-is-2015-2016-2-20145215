package ch06;
/**
 * Created by Think on 2016/4/9.
 */

public class Consumer implements Runnable {
    private Clerk clerk;

    public Consumer(Clerk clerk) {
        this.clerk = clerk;
    }

    public void run() {
        System.out.println("消费者开始消耗整数......");
        for(int i = 1; i <= 10; i++) {
            try {
                clerk.getProduct();
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
