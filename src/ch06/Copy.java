package ch06;
/**
 * Created by Think on 2016/4/9.
 */
import java.io.*;
public class Copy {
    public static void main(String[] args)
            throws IOException{
        IO.dump(
                new FileInputStream(args[0]),
                new FileOutputStream(args[1])
        );
    }
}
