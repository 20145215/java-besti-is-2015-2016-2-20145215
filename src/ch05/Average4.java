package ch05;

import java.util.Scanner;

/**
 * Created by Think on 2016/3/29.
 */
public class Average4 {
    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        double sum = 0;
        int count = 0;
        while (true) {
            int number = nextInt();
            if (number == 0) {
                break;
            }
            sum += number;
            count++;
        }
        System.out.printf("平均 %.2f%n", sum / count);
    }

    static int nextInt() {
        String input = console.next();
        while (!input.matches("\\d*")) {
            System.out.println("请输入数字");
            input = console.next();
        }
        return Integer.parseInt(input);
    }
}
