package ch05;

/**
 * Created by Think on 2016/3/29.
 */
public class AutoClosableDemo {
    public static void main(String[] args) {
        try (Resource res = new Resource()) {
            res.doSome();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        } finally {
            System.out.println("finally…");
        }
    }
}

class Resource implements AutoCloseable {
    void doSome() {
        System.out.println("做一些事情");
    }

    @Override
    public void close() throws Exception {
        System.out.println("资源被关闭");
    }
}