package ch05;

/**
 * Created by Think on 2016/3/29.
 */

import static java.lang.System.out;

public class AutoClosableDemo2 {
    public static void main(String[] args) {
        try (ResourceSome some = new ResourceSome();
             ResourceOther other = new ResourceOther()) {
            some.doSome();
            other.doOther();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class ResourceSome implements AutoCloseable {
    void doSome() {
        out.println("做一些事");
    }

    @Override
    public void close() throws Exception {
        out.println("资源 Some 被关闭");
    }
}

class ResourceOther implements AutoCloseable {
    void doOther() {
        out.println("做其他事");
    }

    @Override
    public void close() throws Exception {
        out.println("资源 Other 被关闭");
    }
}