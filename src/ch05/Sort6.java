package ch05;

/**
 * Created by Cai Ye on 2016/3/31.
 */

import java.util.Arrays;
import java.util.List;

import static java.util.Comparator.nullsFirst;
import static java.util.Comparator.reverseOrder;

public class Sort6 {
    public static void main(String[] args) {
        List words = Arrays.asList("B", "X", "A", "M", null, "F", "W", "O", null);
        words.sort(nullsFirst(reverseOrder()));
        System.out.println(words);
    }
}
