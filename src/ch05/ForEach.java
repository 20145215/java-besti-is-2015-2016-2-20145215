package ch05;

/**
 * Created by Cai Ye on 2016/3/30.
 */

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ForEach {
    public static void main(String[] args) {
        List names = Arrays.asList("Justin", "Monica", "Irene");
        forEach(names);
        forEach(new HashSet(names));
        forEach(new ArrayDeque(names));
    }

    static void forEach(Iterable iterable) {
        for (Object o : iterable) {
            System.out.println(o);
        }
    }
}
